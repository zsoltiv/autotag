from setuptools import *

setup(
    name="autotag",
    version="1.1.0",
    description="automatic music tagging script",
    url="https://gitlab.com/zsoltiv/autotag",
    author="Zsolt Vadasz",
    author_email="zsolt_vadasz@protonmail.com",
    license="MIT",
    packages=find_packages(include=["autotag"]),
    python_requires=">=3.6, <4",
    entry_points={"console_scripts": ["autotag=autotag.__main__:main"]}
)
