# autotag

Python script for tagging music.

## installation
```
pip install -r requirements.txt
pip install .
```

## running:
```
python -m autotag [options] [files]
```

### accurate search:
```python -m autotag --accurate [files]```

### download cover art:
```python -m autotag --cover-art [files]```
