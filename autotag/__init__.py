from __future__ import unicode_literals
import musicbrainzngs
import mutagen

############################################################################
# the program works better if the file follows the form of 'artist - song' #
############################################################################

def write_metadata_to_file(metadata, track_file):
    audio_file = mutagen.File(track_file, easy=True)

    audio_file["title"]  = str(metadata[0])
    audio_file["artist"] = str(metadata[1])
    audio_file["album"]  = str(metadata[2])

    audio_file.save(track_file)

def get_track_metadata(track_file):
    # split the track and the filetype into 2 strings
    data     = track_file.split('/')[-1].split('.')

    track    =    data[0:-1]
    filetype =    data[-1]

    print("track: " + str(track))
    print("filetype: " + filetype)

    # search
    search = musicbrainzngs.search_recordings(query=track)["recording-list"][0]

    # get metadata from search
    title  =                    search["title"]
    artist = search["artist-credit"][0]["name"]
    album  = search["release-list"][0]["title"]

    print("artist: " + artist + "\nalbum: " + album + "\ntitle: " + title)

    # return metadata
    return [title, artist, album]

# TODO: improve accurate search

def get_accurate_track_metadata(track_file):
    data = track_file.split('/')[-1].split('.')


    filetype = data[-0]

    print("filetype: " + filetype)

    # store artist and title in different variables
    track_artist = data[0:-1].split(' - ')[0]
    track_title = data[0:-1].split(' - ')[1]

    # get track artist
    artist_search = musicbrainzngs.search_artists(query=track_artist)["artist-list"][0]
    artist = artist_search["name"]
    
    # get track title
    track_search = musicbrainzngs.search_recordings(query=data[0])["recording-list"]

    title = ""
    album = ""
    # loop through each search result, check if the artist matches, and if it does, we got the right track
    for track in track_search:
        if track["artist-credit"][0]["name"] == artist:
            title = track["title"]
            album = track["release-list"][0]["title"]
            break

    print("found metadata:\nartist: " + artist + "\ntitle: " + title)
    
    # otherwise we set the title to the title we extracted from the file name
    if title == "":
        print("error: failed to get track title")
        title = track_title
    if album == "":
        print("error: failed to get track album")

    return [title, artist, album]

def get_album_cover_art(track_file):
        # get album
        album = get_track_metadata(track_file)[2]

        for release in musicbrainzngs.search_releases(query=album)["release-list"]:
            album_id = release["id"]
            try:
                for image in musicbrainzngs.get_image_list(album_id)["images"]:
                    if image["front"] is True:
                        filename = image["image"].split("/")[-1]
                        data = musicbrainzngs.get_image(album_id, "front", None, "release")

                        open(filename, "wb").write(data)
                        # break the loop if we find a cover art
                        break
            except musicbrainzngs.ResponseError:
                print("failed to get album cover art")
