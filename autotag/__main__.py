#!/usr/bin/python

import autotag
import sys

############################################################################
# the program works better if the file follows the form of 'artist - song' #
############################################################################

def main():
    # initialize musicbrainzngs
    autotag.musicbrainzngs.set_useragent("autotag", "1.0.1")

    accurate_search = False
    cover_art       = False

    for arg in sys.argv[1::]:
        if arg.startswith("-"):
            if arg == "--accurate" or arg == "-a":
                accurate_search = True
            elif arg == "--cover-art" or arg == "-c":
                cover_art = True
            else:
                print("invalid option: " + arg)
                sys.exit(1)
        else:
            if cover_art == False:
                if accurate_search == True:
                    autotag.write_metadata_to_file(autotag.get_accurate_track_metadata(arg), arg)
                else:
                    autotag.write_metadata_to_file(autotag.get_track_metadata(arg), arg)
            else:
                autotag.get_album_cover_art(arg)

if __name__ == "__main__":
    main()
